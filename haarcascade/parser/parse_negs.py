import os
import urllib.request
'''
# Save all images from list of urls
images = []
with open('./haarcascade/parser/urls.txt', 'r') as file:
    for num, url in enumerate(file.readlines()):
        try:
            urllib.request.urlretrieve(url, "neg/neg_{}.jpg".format(num))
        except Exception as e:
            print(e)
            print('Fail')
            pass

# Remove all images that are broken
for image in os.listdir('neg/'):
    file = os.getcwd() + '\\neg\\' + image
    print(file)
    if os.path.getsize(file) == 2051:
        os.remove(file)
    else:
        pass
'''
# Create dat file from files in negative images folder
with open('negatives.dat', 'w') as descriptions:
    for image in os.listdir('neg/'):
        file = 'neg\\' + image + '\n'
        descriptions.write(file)
    

# Run in haarcascade folder
# C:\Users\i331722\AppData\Local\OpenCV\opencv\build\x64\vc14\bin\opencv_createsamples.exe -img hc_0\star.jpg -num 1000 -bg hc_0\negatives.dat -vec samples.vec -maxxangle 0.6 -maxyangle 0 -maxzangle 0.3 -maxidev 100 -bgcolor 0 -bgthresh 0 -w 20 -h 20
# haartraining -data haarcascade -vec samples.vec -bg negatives.dat -nstages 20 -nsplits 2 -minhitrate 0.999 -maxfalsealarm 0.5 -npos 7000 -nneg 3019 -w 20 -h 20 -nonsym -mem 512 -mode ALL