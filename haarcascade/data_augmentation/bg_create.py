import cv2
import numpy as np
from random import randint

class Creator(object):
    def __init__(self, dimensions_x_y, number_of_samples, darkness_of_background = 127):
        self.number_of_samples = number_of_samples
        self.darkness_of_background = darkness_of_background
        self.dimensions_x_y = dimensions_x_y
        self.slots = ['background', '__back', '__noise', '__objects']
        
    def background(self):
        for iteration in range(self.number_of_samples):
            self.process(iteration)
    
    def process(self, number):
        background_image = self.__overlay(self.__back(), self.__noise())
        # cv2.imshow('image', background_image)
        cv2.imwrite('./background_random_images/image{}.jpg'.format(number), background_image)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

        return background_image

    def __back(self):
        image = np.ones((self.dimensions_x_y))
        color = randint(0,127)
        image = np.dot(image, color)
        # print('Color:{}.\nImage:\n{}'.format(color, image))

        return image
    
    def __noise(self):
        noise = np.random.randint(low = 0, high = 255, size = self.dimensions_x_y)
        
        return noise

    def __objects(self):
        pass

    def __overlay(self, background_image, noise):
        merged_layers = np.add(background_image, noise)

        return merged_layers

cr = Creator(dimensions_x_y = (100,100), number_of_samples = 1000, darkness_of_background = 10)
cr.background()