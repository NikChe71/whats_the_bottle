import sys
sys.path.append("..")

from neural.net import classifier
from text_classifier import title_classifier

if __name__ == "__main__":
    url = './LocalFeatures/1.jpg'
    bottle_recogniser = classifier.Recogniser()
    _, images = bottle_recogniser.bottle_finder(url)
    bottle_recogniser.validate()
    for image in images:
        title_classifier.Stencil(image_array = image).read_title()