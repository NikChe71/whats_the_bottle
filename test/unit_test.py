import os
import sys
import unittest
import collections


class TestBottleNet(unittest.TestCase):

    def test_loading(self):
        sys.path.append('../')
        loading_completed = False
        
        try:
            from neural.net import classifier
            print('Library imported successfully')
            bottle_recogniser = classifier.Recogniser()
            loading_completed = True
            print("Loading test completed successfully.")
        
        except Exception as e:
            pass
            
        self.assertEqual(loading_completed, True)
        #self.fail("Loading test failed.")
    
    def test_how_many_bottles(self):
        sys.path.append('../')
        from neural.net import classifier
        bottle_recogniser =  classifier.Recogniser()
        
        images = [
                '../LocalFeatures/0.jpg',
                '../LocalFeatures/2.jpg'
                ]

        for image in images:
            list_of_predictions = bottle_recogniser.bottle_finder(image)[0]
        
        predictions = collections.Counter(list_of_predictions)

        print('Predictions: {} bottles.'.format(predictions['bottle']))

        self.assertEqual(predictions['bottle'], 2)

    def test_how_template_works(self):
        sys.path.append('../')
        from neural.net import classifier
        bottle_recogniser = classifier.Recogniser()
        
        images = [
                '../LocalFeatures/1.jpg',
                '../LocalFeatures/5.jpg'
                ]

        for image in images:
            list_of_predictions = bottle_recogniser.bottle_finder(image)[0]

        result = 0

        bottle_recogniser.validate()

        self.assertEqual(result, 0)

if __name__ == "__main__":
    try:
        unittest.main()
    except SystemExit as e:
        pass
