import numpy as np
import cv2
from matplotlib import pyplot as plt
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--image')
args = parser.parse_args()
#img = cv2.imread('Bottle.JPG',0)
img = cv2.imread(args.image,0)
# Initiate STAR detector
orb = cv2.ORB_create()

# find the keypoints with ORB
kp = orb.detect(img,None)

# compute the descriptors with ORB
kp, des = orb.compute(img, kp)

# draw only keypoints location,not size and orientation
img2 = cv2.drawKeypoints(img,kp,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
cv2.imshow('img2',img2)
cv2.waitKey(0)
cv2.destroyAllWindows()
#plt.imshow(img2),plt.show()
