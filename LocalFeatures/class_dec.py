class A():
    def __init__(self):
        pass
    
    def func0(self):

        print('func0 body')

        def foo(f):
            def wrapper(*args, **kwargs):
                print('foo body')

                result = f(*args, **kwargs)

                print('foo stop')

                return result
            return wrapper
        
        @foo
        def roo():
            print('roo body')

        roo()

a = A()
a.func0()
