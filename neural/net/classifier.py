#import keras
import os
import sys
import numpy as np
import cv2
from matplotlib import pyplot as plt
from skimage.measure import compare_ssim


class Recogniser(object):

    '''
    Initialising all special characteristics of our class instance
    '''
    def __init__(self, model_name = None):
        # Variable for storing model_name
        # Parameter for finetuning algorithm
        self.model_name = model_name

        # Method for loading model by model name
        self.load_model()
        __slots__ = [self.bottle_finder, self.load_model, self.validate]
        self.prediction_threshold = 0.8
        self.answers = []
        # Object Of Interest
        self.OOI = 'bottle'
        self.bottles = []
        self.logo = None

    '''
    Method that eats image,
    proceeds in neural net all objects on it
    and gives the result by drawing the recatangle on image
    '''
    def bottle_finder(self, image_file):
    
        '''
        Adding an handler that will convert
        link to an image array
        or
        will pass, if user appends an array instead of link,
        and saves image array into variable
        '''
        if isinstance(image_file, np.ndarray) == True:
            image = image_file
        elif isinstance(image_file, str) == True:
            image = cv2.imread(image_file)

        self.load_model()
        
        blob = cv2.dnn.blobFromImage(
            # input array image
            cv2.resize(image, (self.input_width, self.input_height)),
            # scale factor
            self.input_scale_factor,
            # Size of image
            (self.input_width, self.input_height),
            # Mean value of one channel (127 if channel has 255 levels)
            (self.mean_channel_value, self.mean_channel_value, self.mean_channel_value),
            # Parameter that will convert images in original channel order to opencv channel order
            self.swap_RB_channels,
            # Parameter that will cropp image if its size is larger than predefined
            self.crop)
        #blob = cv2.dnn.blobFromImage(image, 1, (224, 224), (104, 117, 123))
        
        (h, w) = image.shape[:2]
        self.net.setInput(blob)
        detections = self.net.forward()
        
        rect_colors = np.random.uniform(0, 255, size=(len(self.list_of_classes), 3))

        for confidence_number in np.arange(0, detections.shape[2]):
           prediction = detections[0, 0, confidence_number, 2]
           print(prediction)
           
           if prediction >= self.prediction_threshold:
               class_number = int(detections[0, 0, confidence_number, 1])
               
               if class_number in self.list_of_classes:
                    # if self.list_of_classes[class_number] == self.OOI:
                    print("{}: {:.2f}%".format(self.list_of_classes[class_number], prediction * 100))
                    box = detections[0, 0, confidence_number, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")
                    cv2.rectangle(image, (startX, startY), (endX, endY),
                            rect_colors[class_number], 2)
 
                    self.answers.append(self.list_of_classes[class_number])
                    self.bottles.append(image[startY : endY, startX : endX])
                    
                    print(len(self.bottles))

        cv2.imshow('image', image)
        cv2.waitKey()
        cv2.destroyAllWindows()

        return self.answers, self.bottles

    '''
    Method that appends all data from configs
    and loads specific caffe model in RAM.
    '''
    def load_model(self):
        
        if self.model_name == None:
            self.model_name = "./neural/net/mobilenetssd_stable/MobileNetSSD_deploy"
        
        self.net = cv2.dnn.readNetFromCaffe(self.model_name + ".prototxt", self.model_name + ".caffemodel")
        self.list_of_classes = {
            0: "background", 
            1: "aeroplane",
            2: "bicycle",
            3: "bird",
            4: "boat",
            5: "bottle",
            6: "bus",
            7: "car",
            8: "cat",
            9: "chair",
            10: "cow",
            11: "dinningtable",
            12: "dog",
            13: "horse",
            14: "motorbike",
            15: "person",
            16: "pottedplant",
            17: "sheep",
            18: "sofa",
            19: "train",
            20: "tvmonitor"
        }

        # Opencv swaps image channels "from <-> to":
        # Red Green Blue <-> Blue Green Red
        self.swap_RB_channels = False
        self.crop = False
        self.mean_channel_value = 255.0 / 2.0

        # Network appends only photos of fixed width and height sizes
        # So we add this sizes into variables
        self.input_width = 300
        self.input_height = 300
        self.width_height_ratio = self.input_width / float(self.input_height)
        self.input_scale_factor = 0.007843

    def validate(self):
        
        # Wrapper that will execute template function inside
        # and then find all features on template
        def get_orb_features_wrapper(function):
            
            def wrapper(*args, **kwargs):
                # Cast function that will find objects by template
                print('Getting all images that match by template')
                result_images = function(*args, **kwargs)
                
                # Init SIFT detector
                sift = cv2.xfeatures2d.SIFT_create()

                # Init FLANN matcher parameters
                FLANN_INDEX_KDTREE = 0
                index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
                search_params = dict(checks=50)   # or pass empty dictionary

                # find the keypoints and descriptors with SIFT
                kp1, des1 = sift.detectAndCompute(self.logo,None)

                print('Applying ORB on finded templates.')
                print('Num of images: {}.'.format(len(result_images)))
                for img in result_images:

                    print('Image shape: ', img.shape)

                    # Transform image to grayscale
                    #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                    # compute the descriptors on template with ORB
                    kp2, des2 = sift.detectAndCompute(img,None)

                    '''
                    return ((kp1, des1), (kp2, des2))
                    '''

                    # applying FLANN matcher to count similar keypoints of logo and loaded bottle image
                    flann = cv2.FlannBasedMatcher(index_params,search_params)

                    # List of matches
                    matches = flann.knnMatch(des1,des2,k=2)

                    # Need to draw only good matches, so create a mask
                    matchesMask = [[0,0] for i in range(len(matches))]

                    # ratio test as per Lowe's paper
                    for i,(m,n) in enumerate(matches):
                        if m.distance < 0.7*n.distance:
                            matchesMask[i]=[1,0]

                    draw_params = dict(matchColor = (0,255,0),
                                    singlePointColor = (255,0,0),
                                    matchesMask = matchesMask,
                                    flags = 0)

                    print('Number of descriptors: {}\nNumber of descriptors: {}\nNumber of matches: {}'.format(len(des1),len(des2),len(matches)))

                    img3 = cv2.drawMatchesKnn(self.logo,kp1,img,kp2,matches,None,**draw_params)

                    plt.imshow(img3,); plt.show()

            return wrapper

        @get_orb_features_wrapper
        def find_template(bottle, template = './LocalFeatures/logo0.png'):
            
            result_images = []

            template = cv2.imread(template)
            template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY); self.logo = template
            bottle = cv2.cvtColor(bottle, cv2.COLOR_BGR2GRAY)

            w, h = template.shape[::-1]
            y, x = bottle.shape

            print('Bottle sizes: x={}, y={}.'.format(x,y))
            #cv2.imshow('', bottle); cv2.waitKey(); cv2.destroyAllWindows()

            # Scaling image to find stiker with template algorithm
            for scale_ratio in range(100, 40, -10):
                # Scaling bottle
                bottle = cv2.resize(bottle, (0,0), fx=scale_ratio/100, fy=scale_ratio/100)

                try:
                    # Applying template to image
                    result = cv2.matchTemplate(bottle, template, cv2.TM_CCOEFF_NORMED)

                    # Bad/Best results of matching
                    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
                    bottom_right = (max_loc[0] + w, max_loc[1] + h)

                    result_images.append(bottle[max_loc[1]:max_loc[1] + h, max_loc[0]:max_loc[0] + w])

                    # print('Max loc: {},\nbottom right: {}'.format(max_loc, bottom_right))
                    bottle_draw = bottle.copy()
                    cv2.rectangle(bottle_draw, max_loc, bottom_right, 255, 2)
                    cv2.imshow('template', bottle_draw)
                    cv2.waitKey()
                    cv2.destroyAllWindows()
                except Exception as e:
                    print(e)
                    pass
            
            return result_images

        for num, i in enumerate(self.bottles):
            print('Bottle #{}'.format(num))
            find_template(i)


    def get_orb_features(self, logo, img):
        orb = cv2.ORB_create()

        # find the keypoints with ORB
        kp1 = orb.detect(logo,None)
        kp2 = orb.detect(img,None)

        # compute the descriptors with ORB
        kp1, des1 = orb.compute(logo, kp1)
        kp2, des2 = orb.compute(img, kp2)

        return ((kp1, des1), (kp2, des2))

    def get_sift_features(self, logo, img):
        # Initiate SIFT detector
        sift = cv2.xfeatures2d.SIFT_create()
        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(logo,None)
        kp2, des2 = sift.detectAndCompute(img,None)
        return ((kp1, des1), (kp2, des2))


    def sim_score(self, img0, img1):
        score = compare_ssim(img0, img1, full=True)
        return score


# bottle_recogniser = Recogniser()
# bottle_recogniser.bottle_finder('./LocalFeatures/1.jpg')
# bottle_recogniser.validate()