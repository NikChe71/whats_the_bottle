import re
import cv2
import PIL
import time
import random
import base64
import requests
import pytesseract
import numpy as np

class Stencil(object):
    def __init__(self, image_url = None, image_array = None):
        if image_url == None:
            self.image = image_array
        else:
            self.image = cv2.imread(image_url)

        self.crop_parameters = []
        pytesseract.pytesseract.tesseract_cmd = r'C:\Users\i331722\Documents\CODE\Python\Tesseract\Tesseract-OCR\tesseract.exe'

    def configure(self):
        # Loading file with parameters
        # for tuning algorithm on the fly
        with open('config.txt') as file:
            lines = file.readlines()
            for line in lines:
                parameter = float(re.findall('\d+', line)[0]) / 100
                self.crop_parameters.append(parameter)
            
        left, right, upper, bottom = self.crop_parameters
        return left, right, upper, bottom

    def crop_title(self):
        # Accepting config parameters from method
        left, right, upper, bottom = self.configure()
        # Getting shape of image
        y, x, depth = np.shape(self.image)
        print('X: {}, Y: {}, Depth: {}'.format(x,y,depth))
        # Map stencil on image
        top = int(0+upper*y); bottom = int(y-bottom*y)
        left = int(0+left*x); right = int(x-right*x)
        # Cut image right by the stencil
        title = self.image[top:bottom, left:right]
        
        # Preprocess image
        #image = self.preprocess_image(title)
        
        # Passing image without preprocessing
        image = title

        # Show cropped image
        #cv2.imshow('Title stencil', title)
        #cv2.waitKey()
        #cv2.destroyAllWindows()
        return image

    def preprocess_image(self, image):
        # Gray scale image
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Find edges
        #image = cv2.Canny(image, 100, 200)

        # Sharpen image
        # Set positive and negative values for sharp kernel
        minus = -1; plus = 9
        # Creating sharp kernel array
        sharp_kernel = np.array([[minus, minus, minus],
                [minus, plus, minus],
                [minus, minus, minus]])
        # Applying kernel therefor sharpening image
        # Numerous iterations
        iterations = 1
        for i in range(iterations):
            image = cv2.filter2D(image, -1, sharp_kernel)

        # Threshold it
        _, image = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY_INV)
        
        # Erode it
        erode_kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
        # Applying erode kernel to image
        image = cv2.erode(image, erode_kernel, iterations=1)

        # Dilate it
        image = cv2.dilate(image, erode_kernel, iterations=1)

        # Contrast image
        gain = -1
        bias = 0
        image = cv2.convertScaleAbs(image, alpha=gain, beta=bias)

        # Smoothing edges
        # Creating blurred image
        blur = cv2.GaussianBlur(image,(3,3),0)
        # Blending blured and original images in one image
        # After each image goes percent of opacity
        # Last parameter is gamma.
        # Scalar value that will be added to each pixel 
        # on both images.
        image = cv2.addWeighted(blur, 0.6, image, 0.4, 50)
        
        # Show preprocessed image
        #cv2.imshow('Preprocessed image', image)
        #cv2.waitKey()
        #cv2.destroyAllWindows()
        return image

    def read_title(self):
        image = self.crop_title()
        cv2.imshow('Preprocessed image', image)
        cv2.imwrite('preprocessed_images/{}.jpg'.format(random.randint(0, 100)), image)
        cv2.waitKey()
        cv2.destroyAllWindows()
        try:
            title = pytesseract.image_to_string(image, lang='rus')
            #print(title)
            if title == '':
                title = '_____'
            with open('result.txt', 'a', encoding='UTF-8') as file:
                file.write('{}\n{}\n'.format(title, time.asctime()))
            if len(title) > 0:
                print('\n*** Success ***\n')
            else:
                print('\n*** Cannot recognise text on image.( ***\n')
        except Exception as e:
            return RuntimeError('e')
        return title

    def sap_power(self):
        url = "https://sandbox.api.sap.com/ml/scenetextrecognition/scene-text-recognition"

        headers = {
            "APIKey" : "EltkXHjLOtBCTmurAqSGZ382WwkTOwhP"
        }

        files = {
            #"files" : open(r'C:\Users\i331722\Documents\CODE\Python\Alvisa\preprocessed_images\42.jpg', 'rb')
            "files" : open(r'.\preprocessed_images\42.jpg', 'rb')
        }

        r = requests.post(url = url, files = files, headers = headers)
        print(r.text)

# image_url = r'C:\Users\i331722\Pictures\ObjectRecognition\Alco\example_04.jpg'
# image_url = r'C:\Users\i331722\Pictures\images\IMG-20181205-WA0035.jpg'
# Stencil(image_url).read_title()